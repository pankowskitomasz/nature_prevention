import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionS1Component } from './mission-s1.component';

describe('MissionS1Component', () => {
  let component: MissionS1Component;
  let fixture: ComponentFixture<MissionS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissionS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
