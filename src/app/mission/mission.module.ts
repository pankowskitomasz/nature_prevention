import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MissionRoutingModule } from './mission-routing.module';
import { MissionComponent } from './mission/mission.component';
import { MissionS1Component } from './mission-s1/mission-s1.component';
import { MissionS2Component } from './mission-s2/mission-s2.component';
import { MissionS3Component } from './mission-s3/mission-s3.component';


@NgModule({
  declarations: [
    MissionComponent,
    MissionS1Component,
    MissionS2Component,
    MissionS3Component
  ],
  imports: [
    CommonModule,
    MissionRoutingModule
  ]
})
export class MissionModule { }
