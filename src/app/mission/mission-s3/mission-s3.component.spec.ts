import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionS3Component } from './mission-s3.component';

describe('MissionS3Component', () => {
  let component: MissionS3Component;
  let fixture: ComponentFixture<MissionS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissionS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
