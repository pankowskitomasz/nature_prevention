import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionS2Component } from './mission-s2.component';

describe('MissionS2Component', () => {
  let component: MissionS2Component;
  let fixture: ComponentFixture<MissionS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissionS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
