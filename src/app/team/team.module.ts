import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from './team-routing.module';
import { TeamComponent } from './team/team.component';
import { TeamS1Component } from './team-s1/team-s1.component';
import { TeamS2Component } from './team-s2/team-s2.component';


@NgModule({
  declarations: [
    TeamComponent,
    TeamS1Component,
    TeamS2Component
  ],
  imports: [
    CommonModule,
    TeamRoutingModule
  ]
})
export class TeamModule { }
