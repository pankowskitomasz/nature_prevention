import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamS2Component } from './team-s2.component';

describe('TeamS2Component', () => {
  let component: TeamS2Component;
  let fixture: ComponentFixture<TeamS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
