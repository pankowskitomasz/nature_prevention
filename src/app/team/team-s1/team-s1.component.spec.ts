import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamS1Component } from './team-s1.component';

describe('TeamS1Component', () => {
  let component: TeamS1Component;
  let fixture: ComponentFixture<TeamS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
